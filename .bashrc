#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export MANWIDTH="80"

export VISUAL="w3m"

# Mutt Sending mails -
# https://wiki.archlinux.org/index.php/Mutt#Sending_mails_from_mutt
export EDITOR="vim"

export TESSDATA_PREFIX=$HOME/.tessdata

if [[ -f $HOME/.usr/local/bash_functions ]]; then
    source $HOME/.usr/local/bash_functions
fi

# manual page - Page width
# https://wiki.archlinux.org/index.php/man_page#Online_man_pages
man() {
    local width=$(tput cols)
    [[ $width -gt $MANWIDTH ]] && width=$MANWIDTH
    env MANWIDTH=$width \
    man "$@"
}

# Git - Bash completion
# https://wiki.archlinux.org/index.php/Git#Git_Prompt
source /usr/share/git/completion/git-completion.bash

# Auto "cd" when entering just a path
# https://wiki.archlinux.org/index.php
#       /bash#Auto_"cd"_when_entering_just_a_path
shopt -s autocd

# Git - Git prompt
# https://wiki.archlinux.org/index.php/Git#Git_Prompt
source /usr/share/git/completion/git-prompt.sh
PS1="\u@\h:\W\$(__git_ps1 '(%s)') % "

# Enable servername capability in vim
# https://vim.wikia.com/wiki/Enable_servername_capability_in_vim
alias vim="vim --servername VIM"

# fzf - bash
# https://wiki.archlinux.org/index.php/Fzf#bash
source /usr/share/fzf/key-bindings.bash
source /usr/share/fzf/completion.bash

# Rust Programming By Example - Getting to know Rust
# https://subscription.packtpub.com/book/application_development
#       /9781788390637/1/ch01lvl1sec10/getting-to-know-rust#copyCode-2
source ~/.cargo/env
# Which is the same as executing the following
export PATH="$HOME/.cargo/bin:$PATH"

source /etc/profile.d/jre.sh
