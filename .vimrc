" vimtex-clientserver
" github.com/lervag/vimtex/blob/master/doc/vimtex.txt#L332
if empty(v:servername) && exists('*remote_startserver')
    call remote_startserver('VIM')
endif

let g:w3m#command = 'w3m'

let g:vimtex_view_method = 'zathura'

let g:WMGraphviz_dot = "dot"
let g:WMGraphviz_output = "svg"

" github.com/tpope/vim-pathogen#runtime-path-manipulation
execute pathogen#infect()
Helptags
filetype plugin indent on

" vim.wikia.com/wiki/Converting_tabs_to_spaces
set tabstop=4
set shiftwidth=4
set expandtab

" vim.wikia.com/wiki/How_to_turn_off_all_colors
syntax off

" highlight a specific column ; vim.wikia.com/wiki/Highlight_long_lines
set colorcolumn=80

" Highlighting search matches
" vim.wikia.com/wiki/Highlight_all_search_pattern_matches
set hlsearch

" Avoid the escape key, Mappings ; vim.wikia.com/wiki/Avoid_the_escape_key
imap jk <Esc>
imap kj <Esc>
imap kl <Esc>
imap lk <Esc>

" Changing gutter column width ; vim.wikia.com/wiki/Display_line_numbers
highlight LineNr term=bold ctermfg=black

" vim.wikia.com/wiki/Converting_LANG_to_UTF-8
set fileencodings=utf-8

" wiki.archlinux.org/index.php/mutt#Configuring_editors_to_work_with_mutt
au BufRead /tmp/mutt-* set tw=72

augroup filetypedetect
" Mail
autocmd BufRead,BufNewFile *mutt-*              setfiletype mail
augroup END
