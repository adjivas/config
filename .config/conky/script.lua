--this is a lua script for use in conky
require 'cairo'

function conky_main()
    cpu = tonumber(conky_parse("${cpu}"))

    local function rgb_to_r_g_b(colour,alpha)
        return ((colour / 0x10000) % 0x100) / 255.,
               ((colour / 0x100) % 0x100) / 255.,
                (colour % 0x100) / 255., alpha
    end

    if conky_window == nil then return end
    local cs = cairo_xlib_surface_create(
        conky_window.display,
        conky_window.drawable,
        conky_window.visual,
        conky_window.width,
        conky_window.height
    )
    local cr = cairo_create(cs)

    if cpu > 90 then
        cairo_set_source_rgb(cr, 255, 0, 0)
    end
    center_x = 100
    center_y = 100
    radius = 20
    start_angle = 0
    end_angle = 180
    cairo_arc (cr,center_x,center_y,radius,start_angle,end_angle)
    cairo_fill (cr)
    cairo_stroke (cr)
    
    start_angle = start_angle-130*math.pi/180
    end_angle = -50*math.pi/180
    radius = radius + 60
    cairo_set_line_width (cr, 1)
    cairo_arc (cr,center_x,center_y,radius,start_angle,end_angle)
    cairo_stroke (cr)
    radius = radius + 5
    cairo_arc (cr,center_x,center_y,radius,start_angle,end_angle)
    cairo_stroke (cr)

    cairo_set_line_cap  (cr, CAIRO_LINE_CAP_ROUND)


    cairo_move_to (cr, 100, 100)
    if cpu < 50 then
        cairo_line_to (cr, 50+cpu, 30-cpu*0.3)
    else
        cairo_line_to (cr, 50+cpu, cpu*0.3)
    end
    cairo_stroke (cr)

    cairo_destroy(cr)
    cairo_surface_destroy(cs)

    return ""
end
